package first.example.com;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Point;

public class First extends Applet implements Runnable {
  int frameNumber = -1;
  int faceForm = -1;
  boolean animBackwards = false;
  int delay = 100;
  
  // value to switch between frames
  int stepAnimTime = 80;

  Thread animatorThread;

  boolean frozen = false;

  // drawing the face
  public void drawSpeakingMouth(Graphics g, int faceForm) {
	  // faceForm increases by one, after the middle decreases by one
	  Point facePos = new Point(50,50);
	  int startWidthFace = 50;
	  int startHeightFace = 5;
	  int stepWidthFace = 10;
	  int startPosOffsetX = 5;
	  int startPosOffsetY = 3;
	  int width = startWidthFace - (stepWidthFace * faceForm);
	  int height = startHeightFace * (faceForm + 1);
	  int posOffsetX = startPosOffsetX * faceForm;
	  int posOffsetY = startPosOffsetY * faceForm;
	  g.fillArc(facePos.x + posOffsetX, facePos.y - posOffsetY, width, height, 0, -360);
  }
  
  public void paint(Graphics g) {
	  switch(faceForm) {
	  case 0:
		  drawSpeakingMouth(g, faceForm);
		  break;
	  case 1:
		  drawSpeakingMouth(g, faceForm);
		  if(animBackwards) {
			  faceForm = -1;
			  animBackwards = false;
		  }
		  break;
	  case 2:
		  drawSpeakingMouth(g, faceForm);
		  if(animBackwards) {
			  faceForm = 0;
		  }
		  break;
	  case 3:
	// Mouth is round, middle of animation!
		  drawSpeakingMouth(g, faceForm);
		  faceForm = 1;
		  animBackwards = true;
		  break;
	  default:
		  break;
	  }
  }
  
  public void init() {
    
  }

  public void start() {
    if (!frozen) {
      if (animatorThread == null) {
        animatorThread = new Thread(this);
      }
      animatorThread.start();
    }
  }

  public void stop() {
    animatorThread = null;
  }

  public void run() {
    Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

    long startTime = System.currentTimeMillis();

    Thread currentThread = Thread.currentThread();

    while (currentThread == animatorThread) {
      frameNumber++;
      faceForm++;

      repaint();

      try {
        startTime += delay;
        Thread.sleep(stepAnimTime);
      } catch (InterruptedException e) {
        break;
      }
    }
  }
}